/* global SunCalc */
var App = App || {};
App.sunWidget = function (options) {
    
    "use strict";
    var that = {},
        latitude,
        longitude,
        time,
        date,
        hemisphere,
        model = options.model,
        correctDate = new Date(),
        viewController = options.viewController,
        result,
        parsedDate,
        sunrisePos,
        sunsetPos,
        goldenHourPos,
        solarNoonPos;
        
    function getSunInfo() {
        latitude = model.latitude;
        longitude = model.longitude;
        date = model.date;
        parsedDate = new Date(date);
        correctDate.setDate(parsedDate.getDate());
        time = model.time;
    }
    //https://github.com/mourner/suncalc
    function getPositionInfo() {
        
        sunrisePos = SunCalc.getPosition(result.sunrise, latitude, longitude);
        sunsetPos = SunCalc.getPosition(result.sunset, latitude, longitude);
        goldenHourPos = SunCalc.getPosition(result.goldenHour, latitude, longitude);
        solarNoonPos = SunCalc.getPosition(result.solarNoon, latitude, longitude);
        model.sunposdata.sunrisepos.sunriseAlt = sunrisePos.altitude * 180 / Math.PI;
        model.sunposdata.sunrisepos.sunriseAzi = sunrisePos.azimuth * 180 / Math.PI;
        model.sunposdata.sunsetpos.sunsetAlt = sunsetPos.altitude * 180 / Math.PI;
        model.sunposdata.sunsetpos.sunsetAzi = sunsetPos.azimuth * 180 / Math.PI;
        model.sunposdata.noonpos.noonAlt = solarNoonPos.altitude * 180 / Math.PI;
        model.sunposdata.noonpos.noonAzi = solarNoonPos.azimuth * 180 / Math.PI;
        model.sunposdata.goldenHourpos.goldenAlt = goldenHourPos.altitude * 180 / Math.PI;
        model.sunposdata.goldenHourpos.goldenAzi = goldenHourPos.azimuth * 180 / Math.PI;

    }
    
    function runSunCalc() {
        getSunInfo();
        
        result = SunCalc.getTimes(correctDate, latitude, longitude);
        model.sundata.dusk = (result.dusk.getHours() + ":" + result.dusk.getMinutes());
        model.sundata.dawn = (result.dawn.getHours() + ":" + result.dusk.getMinutes());
        model.sundata.sunrise = (result.sunrise.getHours() + ":" + result.sunrise.getMinutes());
        model.sundata.sunset = (result.sunset.getHours() + ":" + result.sunset.getMinutes());
        model.sundata.solarNoon = (result.solarNoon.getHours() + ":" + result.solarNoon.getMinutes());
        model.sundata.goldenHour = (result.goldenHour.getHours() + ":" + result.goldenHour.getMinutes());
        getPositionInfo();
        
        viewController.updateSunInfo();
    }
    
    that.runSunCalc = runSunCalc;
    that.latitude = latitude;
    that.longitude = longitude;
    that.hemisphere = hemisphere;
    that.date = date;
    that.time = time;
    return that;
};