var App = App || {};
App.entryWidget = function (options) {
    "use strict";
    var that = {},
        searchdialogue = document.querySelector("#inputdiv"),
        latButton = document.querySelector("#latitudebutton"),
        longButton = document.querySelector("#longitudebutton"),
        latinput = document.querySelector("#latitude"),
        longinput = document.querySelector("#longitude"),
        adressinput = document.querySelector("#adress"),
        dateinput = document.querySelector("#date"),
        timeinput = document.querySelector("#time"),
        model = options.model,
        menu = options.menuController;
        
    
    function init() {
        searchdialogue.style.display = "none";
        applyClickListeners();
    }
    
    function applyClickListeners() {
        
        latButton.addEventListener("click", changeLat);
        longButton.addEventListener("click", changeLong);
        latinput.addEventListener("keyup", resetAdress);
        longinput.addEventListener("keyup", resetAdress);
        adressinput.addEventListener("keyup", resetKoord);
    }
    
    //Hemisphären auf Buttonclick per Vorzeichen ändern
    function changeLat() {
        if (latButton.innerHTML === "N") {
            latButton.innerHTML = "S";
            model.hemisphere[0] = "S";
            latinput.value = ("-" + latinput.value);
        } else {
            latButton.innerHTML = "N";
            model.hemisphere[0] = "N";
            latinput.value = Math.abs(latinput.value);
        }
    }
    
    function changeLong() {
        if (longButton.innerHTML === "O") {
            longButton.innerHTML = "W";
            model.hemisphere[1] = "W";
            longinput.value = ("-" + longinput.value);
        } else {
            longButton.innerHTML = "O";
            model.hemisphere[1] = "E";
            longinput.value = Math.abs(longinput.value);
        }
    }
    
    //Eingaben von Koordinaten oder Adresse schließen sich gegenseitig aus
    function resetKoord() {
        menu.showMagnifier();
        longinput.value = "";
        latinput.value = "";
    }
    
    function resetAdress() {
        menu.showMagnifier();
        adressinput.value = "";
    }
    
    
    that.adressinput = adressinput;
    that.latinput = latinput;
    that.longinput = longinput;
    that.dateinput = dateinput;
    that.timeinput = timeinput;
    that.init = init;
    return that;
};