/* global $ */
var App = App || {};
App.elevationWidget = function (options) {
    //https://open.mapquestapi.com/elevation/
    "use stict";
    var that = {},
        adress,
        latitude,
        longitude,
        time,
        date,
        hemisphere,
        viewcontroller = options.viewController,
        API_URL = "https://open.mapquestapi.com/elevation/v1/profile?key=SduFJJDqARsM60MNMwdAlbb5Dn8ikeuI&shapeFormat=raw&latLngCollection={{latitude}}",
        partTwo = ",{{longitude}}",
        fetchURL,
        one, two,
        elevationResult;
    //https://api.jquery.com/jquery.get/
    //Aufruf der mapQuest Elevation API und Abfrage nach Koordinaten aus übergebenen Infos
    function getElevation(info) {
        one = API_URL.replace("{{latitude}}", info.data.lat);
        two = partTwo.replace("{{longitude}}", info.data.lon);
        fetchURL = one + two;
        
        $.getJSON(fetchURL, function(data) {
            elevationResult = data.elevationProfile[0].height;
            
            viewcontroller.updateElevation(elevationResult);
        
        }).done(function() {
            
            
        }).fail(function() {
            alert("Höhensuche fehlgeschlagen, bitte erneut versuchen!");
        }).always(function() {
           
        });
        
    }
    
    that.getElevation = getElevation;
    that.adress = adress;
    that.latitude = latitude;
    that.longitude = longitude;
    that.hemisphere = hemisphere;
    that.date = date;
    that.time = time;
    return that;
};