/* global $ */
var App = App || {};
App.weatherWidget = function () {

    var that = {},
        address,
        latitude,
        lat,
        longitude,
        long,
        time,
        date,
        hemisphere,
        weatherIcon,
        resultdescription,
        API_URL = "http://api.openweathermap.org/data/2.5/forecast?lat={{latitude}}",
        partTwo = "&lon={{longitude}}&units=metric&mode=json&APPID=",
        api_key = "066e75b288f4a447b6dc5b96d91085c9",
        weatherCodes = {
            "thunderstorm": [200, 232],
            "drizzle": [300, 321],
            "rain": [500, 522],
            "snow": [600, 621],
            "clear": [800, 800],
            "clouds": [801, 804]
        },
        currentPlaceData,
        placeDataObject;

       



    function init() {
        weatherIcon = document.getElementById("weather-icon");
        weatherIcon.style.display = "none";
      
    }

    function getWeather(info) {
        var One = API_URL.replace("{{latitude}}", info.data.lat);
        lat = info.data.lat;
        var Two = partTwo.replace("{{longitude}}", info.data.lon);
        long = info.data.lon;
        var Three = One + Two,
            fetchURL = Three + api_key,
            date,
            time,
            hour,
            minute,
            day,
            month,
            year,
            timestamp,
            temperature = null,
            forecastTime = "",
            weatherdescription = null,
            index,
            secondsOfOneAndAHalfHours,
            maximumDate,
            i;

        //https://api.jquery.com/jquery.get/
        $.getJSON(fetchURL, function (data) {
            if (data.cod !== "200") {
                alert("Die Wettersuche ist leider fehlgeschlagen. Bitte noch einmal versuchen oder Seite neu laden.");
                renderView("", "", "Die Wettersuche ist leider fehlgeschlagen.", "");
            } else if (info.data.date === "") {
                weatherdescription = data.list[0].weather[0].description;
                temperature = data.list[0].main.temp;
                forecastTime = data.list[0].dt_txt;
            } else {
                if (info.data.time === "") {
                    info.data.time = "12:00";
                }

                secondsOfOneAndAHalfHours = 60 * 60 * 1.5;

                //Splitting date/time and create timestamp
                date = info.data.date.split("/");
                time = info.data.time.split(":");
                month = parseInt(date[0]) - 1;
                day = parseInt(date[1]);
                year = parseInt(date[2]);
                hour = parseInt(time[0]);
                minute = parseInt(time[1]);
                timestamp = new Date(year, month, day, hour, minute, 0, 0).getTime() / 1000;

                maximumDate = new Date();
                maximumDate.setDate(maximumDate.getDate() + 4);


                if (timestamp < maximumDate.getTime() / 1000) {
                    for (var i = 0; i < data.list.length; i++) {
                        index = i + 1;
                        if (timestamp < data.list[index].dt) {
                            if (timestamp >= data.list[i].dt + secondsOfOneAndAHalfHours) {
                                weatherdescription = data.list[index].weather[0].description;
                                forecastTime = data.list[index].dt_txt;
                                temperature = data.list[index].main.temp.toString();
                                renderView(data.list[index].weather[0].id, temperature, weatherdescription, forecastTime);
                                break;
                            } else if (timestamp < data.list[i].dt) {
                                weatherdescription = data.list[i].weather[0].description;
                                forecastTime = data.list[i].dt_txt;
                                temperature = data.list[i].main.temp.toString();
                                renderView(data.list[i].weather[0].id, temperature, weatherdescription, forecastTime);
                                break;
                            }

                        }

                    }
                } else {
                    alert("Datum zu weit in der Zukunft, um Wetter anzugeben");
                }
            }

            temperature = Math.round(temperature * 10) / 10;
            weather = "Wettervorhersage für " + forecastTime + "<br>";
            weather += weatherdescription;

        }).done(function () {
            
        }).fail(function () {
            alert("Wettersuche fehlgeschlagen, bitte erneut versuchen!");
            
        }).always(function () {
            
        });
    }


    function translateDesc(desc) {
        switch (desc) {
        case "moderate rain":
            resultdescription = "Regen";
            break;
        case "clear sky":
            resultdescription = "Klarer Himmel";
            break;
        case "broken clouds":
            resultdescription = "Bewölkt";
            break;
        case "few clouds":
            resultdescription = "Einige Wolken";
            break;
        case "scattered clouds":
            resultdescription = "Bedeckt";
            break;
        case "shower rain":
            resultdescription = "Regenschauer";
            break;
        case "rain":
            resultdescription = "Regen";
            break;
        case "thunderstorm":
            resultdescription = "Gewitter";
            break;
        case "storm":
            resultdescription = "Sturm";
            break;
        case "mist":
            resultdescription = "Nebel";
            break;
        case "light rain":
            resultdescription = "Leichter Regen";
            break;
        case "overcast clouds":
            resultdescription = "Überwiegend Bewölkt";
            break;
        default:
            resultdescription = "Die Wettersuche ist leider fehlgeschlagen. Bitte noch einmal versuchen oder Seite neu laden.";
           
        }
        return resultdescription;
    }

    function renderView(iconCode, temperature, description, date) {
        var weatherIconCode,
            weatherDate = document.getElementById("weather-date"),
            weatherDescription = document.getElementById("weather-description");
            
        weatherIcon.style.display = "block";
   
        if(date != ""){
            date = processDate(date);
        }
        weatherDate.innerHTML = date;
        description = translateDesc(description);
        weatherDescription.innerHTML = description + ", " + temperature + "°C";

        weatherIconCode = getCorrectWeatherIcon(iconCode);
        weatherIcon.classList = "weather-icon";
        weatherIcon.classList.add("weather-icon-" + weatherIconCode);
        currentPlaceData = new Object();
        currentPlaceData.geoInformation = {long, lat};
        currentPlaceData.date = date;
        currentPlaceData.temperature = temperature +"°C";
        currentPlaceData.weatherDescription = description;
        setCurrentPlaceData(currentPlaceData);
    }

    
    function setCurrentPlaceData(currentPlaceData){
        placeDataObject = currentPlaceData;
    }
    
    function getCurrentPlaceData(){
        return placeDataObject;
    }
    




    function processDate(date) {
        var formattedDate = "",
            splitArray,
            dateArray = [],
            date,
            time = [];
        splitArray = date.split(" ");
        dateArray = splitArray[0].split("-");
        date = dateArray[2] + "." + dateArray[1] + "." + dateArray[0];
        time = splitArray[1];
        time = time.slice(0, -3);
        formattedDate = "Wetter für " + date + ", " + time + " Uhr:";
        return formattedDate;
    }

    function getCorrectWeatherIcon(iconCode) {
        var key1 = iconCode,
            key,
            codes;
        for (key in weatherCodes) {
            if (weatherCodes.hasOwnProperty(key)) {
                codes = weatherCodes[key];
                if (key1 >= codes[0] && key1 <= codes[1]) {
                    iconCode = key;
                }
            }

        }
        return iconCode;
    }

  
    that.getCurrentPlaceData = getCurrentPlaceData;
    that.getWeather = getWeather;
    that.address = address;
    that.latitude = latitude;
    that.longitude = longitude;
    that.hemisphere = hemisphere;
    that.date = date;
    that.time = time;
    that.init = init;
    return that;
};