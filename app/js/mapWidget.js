/* global L */
var App = App || {};
App.mapWidget = function (options) {
    //http://leafletjs.com
    "use strict";
    var that = {},
        model = options.model,
        api_key = "pk.eyJ1IjoiamFtMDkwMTkiLCJhIjoiY2lyYzA3MzI0MDA3bmk5bm4ybHpnZzV0cCJ9.eQ1Xqdza0IQWg5rdaI3Qgg",
        project_id = "jam09019.10jf7pp7",
        mymap,
        marker,
        viewController = options.viewController,
        menu = options.menuController,
        panoramio = options.panoramio;
    
    function initMap() {
        mymap = L.map("mapid").setView([model.longitude, model.latitude], 11);
        
        L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}", {
            attribution: "Map data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors, <a href='http://creativecommons.org/licenses/by-sa/2.0/'>CC-BY-SA</a>, Imagery © <a href='http://mapbox.com'>Mapbox</a>",
            maxZoom: 18,
            id: project_id,
            accessToken: api_key
        }).addTo(mymap);
        mymap.on("click", addMapMarker);
    }
    //Marker bei Klick in die Karte hinzufügen und zugehörige Koordinaten abspeichern
    function addMapMarker(e) {

        if (marker !== undefined) {
            marker.remove();
        }
        marker = L.marker(e.latlng).addTo(mymap);
        model.latitude = e.latlng.lat;
        model.longitude = e.latlng.lng;
        viewController.updateCoordinates();
        viewController.clearAdress();
        menu.showMagnifier();
        panoramio.sendPictureRequest(model.latitude, model.longitude);
    }
    
    //Updaten der Markerposition oder neu Anlegen bei Suche über Eingabefeld
    function addMarker() {
        if (marker !== undefined) {
            marker.remove();
        }
        marker = L.marker([model.latitude, model.longitude]).addTo(mymap);
        mymap.panTo(new L.LatLng(model.latitude, model.longitude));
        panoramio.sendPictureRequest(model.latitude, model.longitude);
    }
    
    that.marker = marker;
    that.addMarker = addMarker;
    that.initMap = initMap;
    return that;
};