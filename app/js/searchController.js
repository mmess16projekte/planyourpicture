/* global $, EventPublisher */
var App = App || {};
App.searchController = function (options) {
    
    "use strict";
    var that = new EventPublisher(),
        searchStartButton,
        entryWidget,
        model,
        menuController = options.menuController,
        perimeterCalc = options.perimeterCalc,
        enter = 13;
        
    
    function init() {
        searchStartButton = document.querySelector("#searchstarterbtn");
        entryWidget = options.entryWidget;
        model = options.model;
        searchStartButton.addEventListener("click", startSearch);
        window.addEventListener("keyup", handleKey);
        $("#date").datepicker();
        $("#time").timepicki({show_meridian: false,
    		min_hour_value: 0,
    		max_hour_value: 23,
    		step_size_minutes: 30,
    		overflow_minutes: true,
    		increase_direction: "up",
    		disable_keyboard_mobile: true});
        
    }
    
    function handleKey(e){
        
        if (e.keyCode == enter){
            startSearch();
        }
    }
    
    
    //Ausführen der Suche und Abspeichern der eingegebenen Daten im Model
    function startSearch (){
        
        model.latitude = entryWidget.latinput.value;
        model.longitude = entryWidget.longinput.value;
        model.adress = entryWidget.adressinput.value;
        if (entryWidget.dateinput.value != ""){
            model.date = entryWidget.dateinput.value;
        }
        model.time = entryWidget.timeinput.value;
        //http://api.jquery.com/jquery.ajax/
        if (entryWidget.adressinput.value != ""){
            $.ajax({
                type: "GET",
                url: "https://api.pickpoint.io/v1/forward?key=zm1etgAe7mMuU5Ueg-zn&q=" + model.adress
            }).done(function (data) {
                var parsedJSON = $.parseJSON(data);
                model.latitude = parsedJSON[0].lat;
                model.longitude = parsedJSON[0].lon;
                
                var info = {
                    lat: model.latitude,
                    lon: model.longitude,
                    date: model.date,
                    time: model.time
                };
                that.notifyAll("searchStarted", info);
                model.updatePerimeterCoordinates(info.lat, info.lon);
                perimeterCalc.getPerimeterElevation(info);
                
            }).done(function() {
            
            }).fail(function() {
                console.log( "error" );
                alert("Höhensuche fehlgeschlagen, bitte erneut versuchen!");
            }).always(function() {
            });
        } else if (model.latitude != "" && model.longitude != "" && model.latitude != "-" && model.longitude != "-"){
            
            var info = {
                lat: model.latitude,
                lon: model.longitude,
                date: model.date,
                time: model.time
            };
            that.notifyAll("searchStarted", info);
            model.updatePerimeterCoordinates(info.lat, info.lon);
            perimeterCalc.getPerimeterElevation(info);
        }
        
        menuController.hideSearchDisplay();
       
    }
    
    that.init = init;
    return that;
};