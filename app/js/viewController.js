/* global CanvasJS */
var App = App || {};
App.viewController = function (options) {
    
    "use strict";
    var that = {},
        model = options.model,
        entryWidget = options.entryWidget,
        timeContainer = document.querySelector("#timecontainer"),
        resultWidget = document.querySelector("#resultdiv"),
        sunplot,
        topoContainer,
        topoHideButton,
        sunHideButton,
        chart1,
        chart2,
        chart3,
        chart4;
    
    function updateCoordinates() {
        entryWidget.latinput.value = model.latitude;
        entryWidget.longinput.value = model.longitude;
    }
    
    function clearAdress() {
        if (entryWidget.adressinput.value) {
            entryWidget.adressinput.value = "";
        }
    }
    
    function updateElevation(elevation) {
        model.elevation = elevation;
        resultWidget.querySelector("#elevation").innerHTML = "Ihr gesuchter Punkt ist auf " +  elevation +  "m";
    }
    
    function updateSunInfo() {
        timeContainer.querySelector("#dusk").innerHTML = "Abenddämmerung ab: " + model.sundata.dusk + " Uhr";
        timeContainer.querySelector("#dawn").innerHTML = "Morgendämmerung ab: " + model.sundata.dawn + " Uhr";
        timeContainer.querySelector("#sunrise").innerHTML = "Sonnenaufgang ab: " + model.sundata.sunrise + " Uhr";
        timeContainer.querySelector("#sunset").innerHTML = "Sonnenuntergang ab: " + model.sundata.sunset + " Uhr";
        timeContainer.querySelector("#goldenHour").innerHTML = "Goldene Stunde ab: " + model.sundata.goldenHour + " Uhr";
        timeContainer.querySelector("#solarNoon").innerHTML = "Mittagssonne ab: " + model.sundata.solarNoon + " Uhr";
        plotSunChart();
    }
    //http://canvasjs.com/html5-javascript-line-chart/
    function plotSunChart() {
        var sunchartdiv = document.getElementById("sunchartcontainer");
        sunHideButton = document.getElementById("sunHideButton");
        sunHideButton.onclick = function () {sunchartdiv.style.visibility = "hidden"; };
        sunchartdiv.style.visibility = "visible";
        sunplot = new CanvasJS.Chart("sunchart", {
            backgroundColor: "rgba(200, 200, 200,0.7)",
            title: {
                text: "Blick nach Süden"
            },
            axisX: {
                labelFontSize: 12,
                valueFormatString: "##°"
            }, axisY: {
                labelFontSize: 12,
                valueFormatString: "####°"
            },
            data: [
                {
                    type: "spline",
                    color: "orange",
                    dataPoints: [
                        {label: "Sonnenaufgang", x: model.sunposdata.sunrisepos.sunriseAzi, y: model.sunposdata.sunrisepos.sunriseAlt},
                        {label: "Mittagssonne", x: model.sunposdata.noonpos.noonAzi, y: model.sunposdata.noonpos.noonAzi + 45},
                        {label: "Goldene Stunde", x: model.sunposdata.goldenHourpos.goldenAzi, y: model.sunposdata.goldenHourpos.goldenAlt},
                        {label: "Sonnenuntergang", x: model.sunposdata.sunsetpos.sunsetAzi, y: model.sunposdata.sunsetpos.sunsetAlt}
                    ]
                }
                
            ]
        });
        
        sunplot.render();
    }
    
    
    function plotGraph() {
        topoContainer = document.getElementById("topoContainer");
        topoContainer.style.visibility = "visible";
        topoHideButton = document.getElementById("topoHideButton");
        topoHideButton.onclick = function () {topoContainer.style.visibility = "hidden"; };
        chart1 = new CanvasJS.Chart("chartContainer_Nord", {
            backgroundColor: "rgba(200, 200, 200,0.7)",
            axisY: {
                labelFontSize: 12
            },
            axisX: {
                labelFontSize: 16
            },
            data: [
                {
                    indexLabelFontSize: 20,
                    indexLabelFontStyle: "bold",
                    type: "line",
                    color: "black",
                    dataPoints: [
        { label: "NW", y: model.perimeterElevation[5]},
        { label: "NNW", y: model.perimeterElevation[9]},
        { label: "Norden", y: model.perimeterElevation[0]},
        { label: "NNO", y: model.perimeterElevation[8]},
        { label: "NO",  y: model.perimeterElevation[4]}
                    ]
                }
            ]
        });

        chart2 = new CanvasJS.Chart("chartContainer_Süd", {
            backgroundColor: "rgba(200, 200, 200,0.7)",
            axisY: {
                labelFontSize: 12
            },
            axisX: {
                labelFontSize: 16
            },
            data: [
                {
                    indexLabelFontSize: 20,
                    indexLabelFontStyle: "bold",
                    type: "line",
                    color: "black",
                    dataPoints: [
        { label: "SW", y: model.perimeterElevation[7]},
        { label: "SSW", y: model.perimeterElevation[12]},
        { label: "Süden", y: model.perimeterElevation[1]},
        { label: "SSO", y: model.perimeterElevation[13]},
        { label: "SO",  y: model.perimeterElevation[6]}
                    ]
                }
            ]
        });
        chart3 = new CanvasJS.Chart("chartContainer_Ost", {
            backgroundColor: "rgba(200, 200, 200,0.7)",
            axisY: {
                labelFontSize: 12
            }, axisX: {
                labelFontSize: 16
            },
            data: [
                {
                    indexLabelFontSize: 20,
                    indexLabelFontStyle: "bold",
                    type: "line",
                    color: "black",
                    dataPoints: [
        { label: "NO", y: model.perimeterElevation[4]},
        { label: "ONO", y: model.perimeterElevation[10]},
        { label: "Osten", y: model.perimeterElevation[2]},
        { label: "OSO", y: model.perimeterElevation[11]},
        { label: "SO",  y: model.perimeterElevation[6]}
                    ]
                }
            ]
        });
        chart4 = new CanvasJS.Chart("chartContainer_West", {
            backgroundColor: "rgba(200, 200, 200,0.7)",
            axisY: {
                labelFontSize: 12
            }, axisX: {
                labelFontSize: 16
            },
            data: [
                {
                    indexLabelFontSize: 20,
                    indexLabelFontStyle: "bold",
                    type: "line",
                    color: "black",
                    dataPoints: [
        { label: "SW", y: model.perimeterElevation[7]},
        { label: "WSW", y: model.perimeterElevation[15]},
        { label: "Westen", y: model.perimeterElevation[3]},
        { label: "WNW", y: model.perimeterElevation[14]},
        { label: "NW",  y: model.perimeterElevation[5]}
                    ]
                }
            ]
        });

        chart1.render();
        chart2.render();
        chart3.render();
        chart4.render();
    }
    
    that.plotSunChart = plotSunChart;
    that.updateSunInfo = updateSunInfo;
    that.plotGraph = plotGraph;
    that.clearAdress = clearAdress;
    that.updateElevation = updateElevation;
    that.updateCoordinates = updateCoordinates;
    return that;
};