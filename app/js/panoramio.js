/* global $, panoramio */
var App = App || {};
App.panoramio = function () {
    "use strict";
    var that = {},
        rectTolerance = 0.005,
        listWidgetWidth = 1300,
        listWidgetHeight = 250,
        longitude,
        latitude,
        maxLatitude,
        maxLongitude,
        minLatitude,
        minLongitude;

    function init() {
        return that;
    }

    //Rechteck, innerhalb dessen Bilder gesucht werden
    function calculateRectangle() {
        maxLatitude = latitude + rectTolerance;
        maxLongitude = longitude + rectTolerance;
        minLatitude = latitude - rectTolerance;
        minLongitude = longitude - rectTolerance;
    }
    
    //Erstellen, Ausführung und Verarbeitung des Requests
    //http://www.panoramio.com/api/widget/api.html#js
    function loadPictures() {
        var requestObject,
            picRequest,
            panoramioOptions,
            widget;

        calculateRectangle();

        requestObject = {
            "set": panoramio.PhotoSet.ALL,
            "rect": {
                "sw": {
                    "lat": minLatitude,
                    "lng": minLongitude
                },
                "ne": {
                    "lat": maxLatitude,
                    "lng": maxLongitude
                }
            }

        };

        picRequest = new panoramio.PhotoRequest(requestObject);

        panoramioOptions = {
            "columns": 5,
            "rows": 1,
            "width": listWidgetWidth,
            "height": listWidgetHeight,
            "croppedPhotos": true,
            "disableDefaultEvents": [panoramio.events.EventType.PHOTO_CLICKED]
        };

        widget = new panoramio.PhotoListWidget("pictureBox", picRequest, panoramioOptions);

        widget.setPosition(0);
        panoramio.events.listen(widget, panoramio.events.EventType.PHOTO_CLICKED, openInLightBox);
    }

    function sendPictureRequest(lat, lng) {

        latitude = lat;
        longitude = lng;
        loadPictures();
    }
    
    //https://webdesign.tutsplus.com/articles/super-simple-lightbox-with-css-and-jquery--webdesign-3528
    function openInLightBox(event) {
        var picture = event.getPhoto();
        var photoURL = picture.Hb;
        var img = $("#panoramioBigPicture");
        var overlay = $("#panoramioOverlay");
        var darkener = $("#panoramioDarkener");
        document.querySelector("#domainlink").href = photoURL;
        document.querySelector("#domainlink").innerHTML = photoURL;
        $(img).attr("src", picture.Xa[0].url);
        $(overlay).width(picture.Xa[0].width);
        $(overlay).height(picture.Xa[0].height);
        $(darkener).show();
        $(overlay).show();
        $(overlay).click(function () {
            openInNewTab(photoURL);
        });
        $(overlay).click(function () {
            $(this).hide();
            $(darkener).hide();
        });
        $(darkener).click(function () {
            $(this).hide();
            $(overlay).hide();
        });
        $(overlay).click(function () {
            openInNewTab(photoURL);
        });

    }

    //http://stackoverflow.com/questions/4907843/open-a-url-in-a-new-tab-and-not-a-new-window-using-javascript
    function openInNewTab(url) {

        var win = window.open(url, "_blank");
        win.focus();
    }

    that.init = init;
    that.sendPictureRequest = sendPictureRequest;
    return that;
};