/* global $, EventPublisher */
var App = App || {};
App.perimeterCalculator = function (options) {
    
    "use strict";
    var that = new EventPublisher(),
        API_URL = "https://open.mapquestapi.com/elevation/v1/profile?key=SduFJJDqARsM60MNMwdAlbb5Dn8ikeuI&shapeFormat=raw&latLngCollection={{latitude}}",
        partTwo = ",{{longitude}}",
        fetchURL,
        model = options.model,
        perimeterResult,
        perimeterLatitude,
        perimeterLongitude,
        first,
        second,
        i;

    //Abrufen der Umgebungskoordinaten aus dem Model und Starten einer neuen Suche
    function getPerimeterElevation(data) {
        
        for (i = 0; i < 16; i+=1){
            perimeterLatitude = model.perimeterCoordinates[i][1];
            perimeterLongitude = model.perimeterCoordinates[i][2];
            
            first = API_URL.replace("{{latitude}}", perimeterLatitude);
            second = partTwo.replace("{{longitude}}", perimeterLongitude);
            fetchURL = first + second;
            
            querySearch(data, i);
        
        }
        
        
    }
    //https://api.jquery.com/jquery.get/
    function querySearch(data, j){
        $.getJSON(fetchURL, function(data) {
            perimeterResult = data.elevationProfile[0].height;
            model.perimeterElevation[j] = perimeterResult;
            //https://api.jquery.com/jQuery.inArray/
            if($.inArray(undefined, model.perimeterElevation) < 0){
                that.notifyAll("dataReady");
            }
        }).done(function() {

        }).fail(function() {
            alert("Höhensuche fehlgeschlagen, bitte erneut versuchen!");
        }).always(function() {
        });
        
    }
    
    that.getPerimeterElevation = getPerimeterElevation;
    return that;
};