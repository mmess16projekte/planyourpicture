var App = App || {};
App.dbConnection = function (options) {
    "use strict";
    var that = new EventPublisher(),
        config = {
            apiKey: "AIzaSyDgh4NMEq9oKR_vHlpZMjxrEyrNXRFB4tw",
            authDomain: "planyourpicture.firebaseapp.com",
            databaseURL: "https://planyourpicture.firebaseio.com",
            storageBucket: "planyourpicture.appspot.com"
        },
        userEmail,
        name,
        place,
        userMailContainer,
        passwordContainer,
        loginButton,
        submitButton,
        loginDialogue,
        logoutDialogue,
        wrongPasswordDialogueCancelButton,
        cancelSignUpButton,
        cancelButton,
        signUpForm,
        signUpButton,
        cancelSignupButton,
        newUserPasswordContainer,
        newUserPassword,
        newUserEmailContainer,
        newUserEmail,
        registerNewUserButton,
        userNameTakenDialogue,
        userNameTakenOKButton,
        wrongPasswordDialogue,
        registrationSuccessButton,
        registrationSuccessDialogue,
        loginStatus,
        userEmail,
        password,
        imagesList,
        picID,
        userPlaces = [],
        id,
        userAccount,
        uploadProgress,
        currentDownloadURL;
  
    function init() {
        firebase.initializeApp(config);
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                id = user.uid;
                loginButton.innerText = "Logout";
                loginButton.addEventListener("click", logoutCurrentUser);
                that.notifyAll("loginSuccess");
            }
        });
        userAccount = options.userAccount;
        setupView();
    }
    
    //View
    function setupView() {
        registerLoginElements();
        registerSignUpElements();
    }
    
    function registerLoginElements() {
        loginDialogue = document.getElementById("logindialogue");
        loginDialogue.style.display = "none";
        loginButton = document.getElementById("login");
        loginButton.addEventListener("click", toggleLoginDialogueDisplay);
        cancelButton = document.getElementById("buttonCancel");
        cancelButton.addEventListener("click", toggleLoginDialogueDisplay);
        userMailContainer = document.getElementById("userName");
        passwordContainer = document.getElementById("password");
        submitButton = document.getElementById("submitbutton");
        submitButton.addEventListener("click", userAuthentication);
        wrongPasswordDialogue = document.getElementById("wrongPasswordDialogue");
        wrongPasswordDialogue.style.display = "none";
        wrongPasswordDialogueCancelButton = document.getElementById("buttonCancelWrongPasswordDialogue");
        wrongPasswordDialogueCancelButton.addEventListener("click", hideWrongPasswordDialogue);
    }
    
    function registerSignUpElements() {
        registerNewUserButton = document.getElementById("newUserButton");
        registerNewUserButton.addEventListener("click", registerNewUser);
        signUpForm = document.getElementById("signupdialogue");
        signUpForm.style.display = "none";
        signUpButton = document.getElementById("signupbutton");
        signUpButton.addEventListener("click", logUserRegisterData);
        cancelSignUpButton = document.getElementById("buttonCancelSignUp");
        cancelSignUpButton.addEventListener("click", toggleSignUpFormDisplay);
        newUserPasswordContainer = document.getElementById("newUserPassword");
        newUserEmailContainer = document.getElementById("newUserEmail");
        userNameTakenDialogue = document.getElementById("userNameTakenDialogue");
        userNameTakenDialogue.style.display = "none";
        userNameTakenOKButton = document.getElementById("userNameTakenDialogue");
        userNameTakenOKButton.addEventListener("click", hideUserNameTakenDialogue);
        registrationSuccessDialogue = document.getElementById("registrationSuccess");
        registrationSuccessDialogue.style.display = "none";
        registrationSuccessButton = document.getElementById("registrationSuccesButton");
        registrationSuccessButton.addEventListener("click", hideRegistrationWasSuccessDialogue);
    }
    
    function userAuthentication() {
        toggleLoginDialogueDisplay();
        userEmail = userMailContainer.value;
        password = passwordContainer.value;
        firebase.auth().signInWithEmailAndPassword(userEmail, password).catch(function (error) {
            if (error.message === "") {
                id = user.uid;
                loginStatus = "on";
            } else {
                alert(error.message);
                loginStatus = "off";
            }
        });
        userMailContainer.value = "";
        passwordContainer.value = "";
    }
    
    function logoutCurrentUser() {
        firebase.auth().signOut().then(function () {
            loginStatus = "off";
        }, function (error) {
        });
        userAccount.logoutUser();
        userPlaces = [];
        loginButton.innerText = "Login";
        loginButton.addEventListener("click", toggleLoginDialogueDisplay);
    }
    
    function toggleLoginDialogueDisplay() {
        if (event.target.parentElement.innerText === "Logout") {
            logoutCurrentUser();
        } else if (loginDialogue.style.display === "block") {
            loginDialogue.style.display = "none";
        } else if (loginDialogue.style.display === "none") {
            loginDialogue.style.display = "block";
        }
    }
    
    //register new user
    function setupUserAccount(email, password) {
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
            var errorMessage = error.message;
            if (error.code === "auth/email-already-in-use") {
                showUserNameTakenDialogue();
            }
            id = user.uid;
            loginStatus = "on";
        });
    }

    function logUserRegisterData() {
        loginDialogue.style.display = "none";
        toggleSignUpFormDisplay();
        newUserPassword = newUserPasswordContainer.value;
        newUserEmail = newUserEmailContainer.value;
        newUserPasswordContainer.value = "";
        newUserEmailContainer.value = "";
        setupUserAccount(newUserEmail, newUserPassword);
    }
    
    function downloadUserData(callback) {
        firebase.database().ref('users/' + id).once('value', function (snapshot) {
            if (snapshot.val() !== null) {
                userPlaces.push(snapshot.val());
                callback();
            }
        });
    }
    
    function fillUserPlacesList() {
        downloadUserData(function () {
            userAccount.fillUserPlacesList(userPlaces);
        });
    }
    
    function hideWrongPasswordDialogue() {
        wrongPasswordDialogue.style.display = "none";
        toggleLoginDialogueDisplay();
    }
    
    function showWrongPasswordDialogue() {
        wrongPasswordDialogue.style.display = "block";
    }
    
    function hideWrongPasswordDialogue() {
        wrongPasswordDialogue.style.display = "none";
    }
    
    function registerNewUser() {
        toggleLoginDialogueDisplay();
        toggleSignUpFormDisplay();
    }
    
    function showRegistrationWasSuccessDialogue() {
        registrationSuccessDialogue.style.display = "block";
    }
    
    function hideRegistrationWasSuccessDialogue() {
        registrationSuccessDialogue.style.display = "none";
    }
    
    function hideUserNameTakenDialogue() {
        userNameTakenDialogue.style.display = "none";
        toggleSignUpFormDisplay();
    }
    
    function toggleSignUpFormDisplay() {
        if (signUpForm.style.display === "block") {
            signUpForm.style.display = "none";
        } else if (signUpForm.style.display === "none") {
            signUpForm.style.display = "block";
        }
    }
    
    function showUserNameTakenDialogue() {
        userNameTakenDialogue.style.display = "block";
    }
    
    function processCurrentPlaceData(currentPlaceData) {
        userAccount.setCurrentPlaceData(currentPlaceData);
    }
    
    function writeCurrentPlaceInDataBase(placeData) {
        var placeName = placeData.name.toString();
        if (placeName.indexOf(".") !== -1) {
            placeName = placeName.replace(".", "");
        }
        firebase.database().ref('users/' + id + '/places/' + placeName).set({
            placeDate:  placeData.date,
            placeWeather: placeData.weather,
            placeGeoInformation: placeData.geoInformation,
            placeNotes: ""
        });
    }
    
    function processUserNotes(userNotes, placeName) {
        var currentPlace = placeName;
        if (currentPlace.indexOf(".") !== -1) {
            currentPlace = currentPlace.replace(".", "");
        }
        firebase.database().ref('users/' + id + '/places/' + currentPlace + '/placeNotes').push({
            userNotes });
        
    }
    
    //Quelle: https://firebase.google.com/docs/storage/web/upload-files
    function storePictureInStorage(file,place) {
		var metadata = {'contentType': file.type};
        var fileName = file.name;
        if(fileName.indexOf(".") !== -1){
            fileName = file.name.replace(".", "");
        }
		var uploadTask = firebase.storage().ref(id+ "/images/" + fileName).put(file,metadata);
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, 
        function(snapshot) {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            uploadProgress = progress;
            setProgressBar();
            if(uploadProgress == 100){
                var downloadURL = uploadTask.snapshot.downloadURL;
                currentDownloadURL = downloadURL;
                createPictureRecord(fileName, downloadURL, place);
            }
        });
	}
    
    
	//Quelle: http://www.w3schools.com/howto/howto_js_progressbar.asp
    function setProgressBar() {
        var bar = document.getElementById("progress-bar"); 
        var timer = setInterval(frame, 1);
        function frame() {
            if (uploadProgress >= 101) {
                clearInterval(id);
            } else {
                bar.style.width = uploadProgress + "%"; 
            }
        }
    }
    
	function createPictureRecord(fileName, url, place){
		firebase.database().ref("users/" +id+"/places/" +place+"/images/" ).push({
			url: url
		});
        displayUploadPic();
	}    
    
    //Quelle: https://firebase.google.com/docs/storage/web/download-files
    function displayUploadPic(){
        imagesList = document.getElementById("images-list");
        var li = document.createElement("li");
        li.innerHTML = "<img class='user-images-list-item' src=" + currentDownloadURL + ">"
        imagesList.appendChild(li);   
    }
    
    function fillUserImagesList(currentUserPlace){
        var urlArray = [];
        imagesList = document.getElementById("images-list");
        imagesList.innerHTML = "";
        for (picID in userPlaces[0].places[currentUserPlace].images) {
            if (picID !== undefined) {
                var picURL = userPlaces[0].places[currentUserPlace].images[picID].url;
                var li = document.createElement("li");
                li.innerHTML = "<img class='user-images-list-item' src=" + picURL + ">"
                imagesList.appendChild(li);   
            }
        }
    }
    
    that.fillUserImagesList = fillUserImagesList;
    that.fillUserPlacesList = fillUserPlacesList;
    that.downloadUserData = downloadUserData;
    that.storePictureInStorage = storePictureInStorage; 
    that.processUserNotes = processUserNotes;
    that.writeCurrentPlaceInDataBase = writeCurrentPlaceInDataBase;
    that.processCurrentPlaceData = processCurrentPlaceData;
    that.init = init;
    return that;
};