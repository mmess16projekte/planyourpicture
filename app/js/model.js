var App = App || {};
App.model = function () {
    
    "use strict";
    var that = {},
        adress,
        latitude = 12.1,
        longitude = 49,
        time,
        date = "04/08/2016",
        hemisphere = ["N", "E"],
        elevation,
        perimeterElevation = [],
        perimeterCoordinates = [],
        dusk, dawn, sunset, sunrise, solarNoon, goldenHour,
        sunsetAlt, sunsetAzi,
        sunriseAlt, sunriseAzi,
        noonAlt, noonAzi,
        goldenAlt, goldenAzi,
        sunsetpos = {sunsetAlt, sunsetAzi},
        sunrisepos = {sunriseAlt, sunriseAzi},
        noonpos = {noonAlt, noonAzi},
        goldenHourpos = {goldenAlt, goldenAzi},
        sundata = {dusk, dawn, sunset, sunrise, solarNoon, goldenHour},
        sunposdata = {sunsetpos, sunrisepos, noonpos, goldenHourpos};
    
    
    //updaten der Umgebungskoordinaten
    function updatePerimeterCoordinates(lat, lon){
        
        perimeterCoordinates[0] = ["N", lat -(-0.001), lon - 0, perimeterElevation[0]];
        perimeterCoordinates[1] = ["S", lat - 0.001, lon - 0, perimeterElevation[1]];
        perimeterCoordinates[2] = ["E", lat - 0, lon - (-0.001), perimeterElevation[2]];
        perimeterCoordinates[3] = ["W", lat - 0, lon - 0.001, perimeterElevation[3]];
        perimeterCoordinates[4] = ["NE", lat - (-0.001), lon - (-0.001), perimeterElevation[4]];
        perimeterCoordinates[5] = ["NW",lat - (-0.001), lon - 0.001, perimeterElevation[5]];
        perimeterCoordinates[6] = ["SE",lat - 0.001, lon - (-0.001), perimeterElevation[6]];
        perimeterCoordinates[7] = ["SW",lat - 0.001, lon -0.001, perimeterElevation[7]];
        perimeterCoordinates[8] = ["NNE",lat - (-0.001), lon - (-0.0005), perimeterElevation[8]];
        perimeterCoordinates[9] = ["NNW",lat - (-0.001), lon - 0.0005, perimeterElevation[9]];
        perimeterCoordinates[10] = ["ENE",lat - (-0.0005), lon - (-0.001), perimeterElevation[10]];
        perimeterCoordinates[11] = ["ESE",lat - 0.0005, lon - (-0.001), perimeterElevation[11]];
        perimeterCoordinates[12] = ["SSW",lat - 0.001, lon - 0.0005, perimeterElevation[12]];
        perimeterCoordinates[13] = ["SSE",lat - 0.001, lon - (-0.0005), perimeterElevation[13]];
        perimeterCoordinates[14] = ["WNW",lat - (-0.0005), lon -0.001, perimeterElevation[14]];
        perimeterCoordinates[15] = ["WSW",lat - 0.0005, lon - 0.001, perimeterElevation[15]];
    }
    
    
    that.sunposdata = sunposdata;
    that.sundata = sundata;
    that.updatePerimeterCoordinates = updatePerimeterCoordinates;
    that.perimeterCoordinates = perimeterCoordinates;
    that.perimeterElevation = perimeterElevation;
    that.elevation = elevation;
    that.adress = adress;
    that.latitude = latitude;
    that.longitude = longitude;
    that.hemisphere = hemisphere;
    that.date = date;
    that.time = time;
    return that;
};