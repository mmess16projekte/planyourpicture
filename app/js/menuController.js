/* global $ */
var App = App || {};
App.menuController = function () {
    
    "use strict";
    var that = {},
        searchDialogue = document.getElementById("inputdiv"),
        searchStartButton = document.querySelector("#searchstarterbtn");
    
    function init() {
        searchStartButton.style.visibility = "hidden";
        searchStartButton.addEventListener("click", hideSearchDisplay);
    }
    
    window.addEventListener("click", handleClick);
    
    function handleClick(event) {   
        if (event.target.id === "search") {
            toggleSearchDisplay();
        }
		else if(event.target.id == "pictUpload")
		{
    if($("#userUpload").is(":visible"))
			{
        $("#userUpload").hide();
    }
			else
			{
        $("#userUpload").show();
    }
		}
    }
    
   
    
    function toggleSearchDisplay() {
        if (searchDialogue.style.display === "block") {
            searchDialogue.style = "display: none;";
        } else if (searchDialogue.style.display === "none") {
            searchDialogue.style.display = "block";
        }
    }
    
    function hideSearchDisplay() {
        if (searchDialogue.style.display === "block") {
            searchDialogue.style = "display: none;";
        }
    }
    
    function showMagnifier() {
        if (searchStartButton.style.visibility === "hidden") {
            searchStartButton.style.visibility = "visible";
        }
    }
    
    that.hideSearchDisplay = hideSearchDisplay;
    that.toggleSearchDisplay = toggleSearchDisplay;
    that.showMagnifier = showMagnifier;
    that.init = init;
    return that;
};