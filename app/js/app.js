var App = App || {};
App.PlanYourPicture = (function () {

    "use strict";
    var that = {},
        mapWidget,
        entryWidget,
        weatherWidget,
        searchController,
        elevationWidget,
        menuController,
        sunWidget,
        viewController,
        userDB,
        perimeterCalc,
        panoramio,
        userData,
        model,
        userAccount,
        userUPL,
        wikiWidget;

    function init() {
        model = new App.model();
        panoramio = new App.panoramio();

        menuController = new App.menuController();
        menuController.init();

        entryWidget = new App.entryWidget({model, menuController});
        entryWidget.init();

        viewController = new App.viewController({model, entryWidget});

        sunWidget = new App.sunWidget({model, viewController});
        mapWidget = new App.mapWidget({model, viewController, panoramio, userUPL, menuController});

        weatherWidget = new App.weatherWidget();
        weatherWidget.init();

        mapWidget.initMap();
       
        userAccount = new App.userAccountView({model, weatherWidget});
        userAccount.init();
        userAccount.addEventListener("initUpload", initUserPicUpload);
        
        
        perimeterCalc = new App.perimeterCalculator({model});
        perimeterCalc.addEventListener("dataReady", plot);
        elevationWidget = new App.elevationWidget({viewController});

        searchController = new App.searchController({model, entryWidget, mapWidget, perimeterCalc, menuController});
        searchController.addEventListener("searchStarted", search);
        searchController.init(model, entryWidget);
        wikiWidget = new App.wikiWidget();
        wikiWidget.init();

        userDB = new App.dbConnection({userAccount});
        userDB.addEventListener("loginSuccess", loadUserData);
        userDB.init();
    }
    
    function loadUserData() {
        userAccount.initUserDB(userDB);
        userData = userDB.getUserData();
    }
    
    function initUserPicUpload(){
        userUPL = new App.pictureUpload({userAccount});
        userUPL.init();
        userUPL.setDBCallback(uploadPicture);    
    }
    
    function search(query) {
        model.updatePerimeterCoordinates();
        weatherWidget.getWeather(query);
        mapWidget.addMarker();
        sunWidget.runSunCalc();
    }

    function plot() {
        viewController.plotGraph();
        viewController.plotSunChart();
    }

    function uploadPicture(file,place) {
        userDB.storePictureInStorage(file,place);
    }

    that.init = init;
    return that;
}());