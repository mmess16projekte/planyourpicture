//Quelle: http://www.w3schools.com/howto/howto_js_fullscreen_overlay.asp[2016-08-04]

var App = App || {};
App.userAccountView = function (options) {
    "use strict";
    var that = new EventPublisher(),
        myPlacesButton,
        userPlacesMenu,
        userPlacesList,
        singleUserPlacesItem,
        sampleEntries,
        model = options.model,
        addPlaceWithCurrentDataButton,
        dateSpace,
        cityNameSpace,
        weatherSpace,
        confirmManuallyAddedPlaceButton,
        placeData,
        weather,
        selectedUserPlace,
        clickedPlace,
        userNotesForSelectedPlace,
        notesList,
        addUserNotes,
        currentOpenedPlaceData,
        userImagesMenu,
        selectedPlaceHeader,
        userImagesList,
        db,
        currentPlaceName;
    
    //setup UI Elemnts
    function init() {
        userPlacesMenu =  document.getElementById("userPlaces");
        userPlacesMenu.style.width = "0px";
        myPlacesButton = document.getElementById("myPlaces");
        myPlacesButton.addEventListener("click", toggleMenuVisibility);
        myPlacesButton.style.visibility = "hidden";
        userPlacesList = document.getElementById("places-list");
        addPlaceWithCurrentDataButton = document.getElementById("add-place-with-current-data-button");
        addPlaceWithCurrentDataButton.addEventListener("click", addPlaceWithCurrentData);
        weather = options.weatherWidget;
        selectedUserPlace = document.getElementById("selected-user-place");
        selectedUserPlace.style.width = "0px";
        var showUserImagesButton = document.getElementById("show-images-button");
        showUserImagesButton.addEventListener("click", toggleUserImagesFormDisplay);
        userImagesMenu = document.getElementById("user-images-menu");
        userImagesMenu.style.width = "0px";
        selectedPlaceHeader = document.getElementById("selected-place-header");
        singleUserPlacesItem = document.getElementById("places-list-entry");
    }
    

    function initUserDB(userDB) {
        myPlacesButton.style.visibility = "visible";
        myPlacesButton.addEventListener("click", toggleMenuVisibility);
        db = userDB;
        db.fillUserPlacesList();
    }
    
    //close User Menu
    function logoutUser() {
        myPlacesButton.style.visibility = "hidden";
        userPlacesMenu.style.width = "0px";
        selectedUserPlace.style.width = "0px";
        userImagesMenu.style.width = "0px";
        userPlacesList.innerHTML = "";
    }
    
    //add single place to user-places-list; register clicklistener to list-items
    function addListItem(placeData) {
        var li = document.createElement("li");
        li.innerHTML = singleUserPlacesItem.innerHTML;
        li.firstElementChild.innerHTML = placeData.name;
        li.addEventListener("click", function (e) {
            var clickedPlace = e.target.innerHTML;
            selectedPlaceHeader.innerHTML = clickedPlace;
            selectedPlaceDialogueToggleVisibility(selectedUserPlace);
            userNotesForSelectedPlace = document.getElementById("notes-input-field");
            addUserNotes = document.getElementById("add-place-notes");
            addUserNotes.addEventListener("click", addUserNotesToSelectedPlace);
        });
        userPlacesList.appendChild(li);
    }
    
    function addPlaceWithCurrentData() {
        var currentPlaceData = weather.getCurrentPlaceData();
        getCurrentPlaceNameFromLatLng(currentPlaceData.geoInformation.lat, currentPlaceData.geoInformation.long, currentPlaceData);
    }
    
    //Quelle: http://stackoverflow.com/questions/6548504/how-can-i-get-city-name-from-a-latitude-and-longitude-point
    function getCurrentPlaceNameFromLatLng(lat, long, currentPlaceData) {
        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long;
        $.getJSON(url, function (data) {
            var adress = data.results[0],
                formattedPlace = adress.formatted_address;
            if (formattedPlace.indexOf("Str.") !== -1) {
                formattedPlace = formattedPlace.replace("Str.", "Strasse");
            }
            processPlaceData(formattedPlace, currentPlaceData, currentPlaceData.geoInformation);
        }).done(function () {
        }).fail(function () {
            alert("Adresse nicht gefunden, bitte erneut versuchen!");
        }).always(function () {
        });
    }
  
    //prepare data-object to be written in database
    function processPlaceData(formattedPlace, currentPlaceData, geoInformation) {
        placeData = {};
        placeData.name = formattedPlace;
        placeData.date = currentPlaceData.date;
        placeData.geoInformation = geoInformation;
        placeData.weather = currentPlaceData.temperature + ", " + currentPlaceData.weatherDescription;
        placeData.notes = "";
        addListItem(placeData);
        db.writeCurrentPlaceInDataBase(placeData);
    }
   
    //add saved user places to placesList
    function fillUserPlacesList(savedUserPlaces) {
        var place;
        singleUserPlacesItem = document.getElementById("places-list-entry");
        for (place in savedUserPlaces[0].places) {
            var li = document.createElement("li");
            li.innerHTML = singleUserPlacesItem.innerHTML;
            li.firstElementChild.innerHTML = place;
            userPlacesList.appendChild(li);
            li.addEventListener("click", function (e) {
                clickedPlace = e.target.innerHTML;
                selectedPlaceHeader.innerHTML = clickedPlace;
                selectedPlaceDialogueToggleVisibility(selectedUserPlace);
                var currentPlace = savedUserPlaces[0].places[clickedPlace];
                weatherSpace = document.getElementById("selected-place-weather");
                weatherSpace.innerHTML = currentPlace.placeWeather;
                dateSpace = document.getElementById("selected-place-date");
                dateSpace.innerHTML = currentPlace.placeDate;
                userNotesForSelectedPlace = document.getElementById("notes-input-field");
                getExistingUserNotes(currentPlace);
                addUserNotes = document.getElementById("add-place-notes");
                addUserNotes.addEventListener("click", addUserNotesToSelectedPlace);
            });
        }
    }
    
    //get existing user notes for selected place and add them to list
    function getExistingUserNotes(currentPlace) {
        var notes;
        notesList = document.getElementById("notes-list");
        notesList.innerHTML = "";
        for (notes in currentPlace.placeNotes) {
            if (notes !== "") {
                var note = currentPlace.placeNotes[notes].userNotes,
                    li = document.createElement("li");
                li.innerHTML = "# " + note;
                notesList.appendChild(li);
            }
        }
    }
    
    //write new user note to database & notes list
    function addUserNotesToSelectedPlace() {
        var userNotes = userNotesForSelectedPlace.value;
        if (userNotes !== "") {
            var placeName = userNotesForSelectedPlace.parentElement.parentElement.firstElementChild.innerHTML;
            userNotesForSelectedPlace.value = "";
            var notesList = document.getElementById("notes-list"),
                li = document.createElement("li");
            li.innerHTML = "# " + userNotes;
            notesList.appendChild(li);
            db.processUserNotes(userNotes, placeName);
        }
    }
    
    function toggleMenuVisibility() {
        if (userPlacesMenu.style.width === "300px") {
            selectedUserPlace.style.width = "0px";
            userPlacesMenu.style.width = "0px";
            userImagesMenu.style.width = "0px";
        } else if (userPlacesMenu.style.width === "0px") {
            userPlacesMenu.style.width = "300px";
        }
    }
    
    function selectedPlaceDialogueToggleVisibility(selectedPlace) {
        if (clickedPlace === currentOpenedPlaceData && selectedPlace.style.width === "300px") {
            selectedPlace.style.width = "0px";
            userImagesMenu.style.width = "0px";
        } else if (selectedPlace.style.width === "0px" || selectedPlace.style.width === "") {
            selectedPlace.style.width = "300px";
        } else if (selectedPlace.style.width === "300px" && clickedPlace !== currentOpenedPlaceData) {
            userImagesMenu.style.width = "0px";
        }
        currentOpenedPlaceData = selectedPlace.firstElementChild.firstElementChild.innerHTML;
    }
    
    function toggleUserImagesFormDisplay() {
        currentPlaceName = event.target.parentElement.parentElement.firstElementChild.innerHTML;
        that.notifyAll("initUpload");
        if (userImagesMenu.style.width === "600px") {
            userImagesMenu.style.width = "0px";
        } else if (userImagesMenu.style.width === "" || userImagesMenu.style.width === "0px") {
            userImagesMenu.style.width = "600px";
            db.fillUserImagesList(currentPlaceName);
        }
    }
    
    function getCurrentPlace() {
        return currentPlaceName;
    }
    
    that.initUserDB = initUserDB;
    that.getCurrentPlace = getCurrentPlace;
    that.logoutUser = logoutUser;
    that.fillUserPlacesList = fillUserPlacesList;
    that.init = init;
    return that;
};