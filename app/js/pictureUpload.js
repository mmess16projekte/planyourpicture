/* global $ */
var App = App || {};
App.pictureUpload = function (options) {
    
    "use strict";
    var that = {},
        writeToDBFNC,
        place,
        userAccountView;

    function init() {
        userAccountView = options.userAccount;
        place = userAccountView.getCurrentPlace();
        $("#userUpload").css("width", "300px").hide();
        $("#picSelectBTN").button().click(function () {
            $("#customPictureInput").click();
        });
        upl_registerEventhandler();
        return that;
    }


    function upl_registerEventhandler() {
        $("#customPictureInput").on("change",
            function (event) {
                var file = event.target.files[0];
                writeToDBFNC(file, place);
            }
        );
    }

    function setDBCallback(cb) {
        writeToDBFNC = cb;
    }
    
    that.init = init;
    that.updatePlace = place;
    that.setDBCallback = setDBCallback;
    return that;
};