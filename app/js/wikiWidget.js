//Quelle: http://www.w3schools.com/howto/howto_js_fullscreen_overlay.asp[2016-08-04]
/* global $ */
var App = App || {};
App.wikiWidget = function () {
    
    "use strict";
    var that = {},
        nextButton,
        previousButton,
        wikicontent,
        wiki,
        wikiButton,
        wikiData,
        page = 0;
    
    
    function init() {
        //https://www.sitepoint.com/jquery-read-text-file/
        wikiData = $.getJSON("./res/tipps.txt");
        wiki = document.getElementById("wiki");
        wiki.style.width = "0%";
        wiki.style.height = "0%";
        wiki.style.visibility = "hidden";
        nextButton = document.getElementById("nextbutton");
        nextButton.addEventListener("click", nextPage);
        previousButton = document.getElementById("previousbutton");
        previousButton.addEventListener("click", previousPage);
        wikiButton = document.getElementById("wikibutton");
        wikiButton.addEventListener("click", toggleMenuVisibility);
        wikicontent = document.getElementById("wikicontent");
        
    }
    
    //Auftrennen der Textzeilen in Array, das auf die Seiten aufgeteilt wird
    function splitWikiData() {
        if (wikiData.length === undefined) {
            wikiData = wikiData.responseText.split("\n");
        }
    }
    
    function nextPage() {
        if (page < wikiData.length - 1) {
            page += 1;
        } else if (page === wikiData.length - 1) {
            page = 0;
        }
        
        wikicontent.innerHTML = wikiData[page];
    }
    
    function previousPage() {
        if (page > 0) {
            page -= 1;
        } else if (page === 0) {
            page = wikiData.length - 1;
        }
        wikicontent.innerHTML = wikiData[page];
    }
    
    
    function toggleMenuVisibility() {
        if (wiki.style.width === "30%") {
            closeWiki();
        } else if (wiki.style.width === "0%") {
            openWiki();
        }
    }
    
    function openWiki() {
        wiki.style.visibility = "visible";
        wiki.style.width = "30%";
        wiki.style.height = "20%";
        splitWikiData();
        wikicontent.innerHTML = wikiData[page];
    }

    function closeWiki() {
        wiki.style.visibility = "hidden";
        wiki.style.width = "0%";
        wiki.style.height = "0%";
    }
    
    that.init = init;
    return that;
};