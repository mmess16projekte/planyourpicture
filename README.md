Zur einfachen Verwendung von PlanYourPicture ist der Start so unkompliziert wie möglich gehalten.
Es sind keine besonderen Installationen notwendig. Die Anwendung kann sofort im Browser über den Link http://homepages.uni-regensburg.de/~grj16524/ benutzt werden.

Es steht Ihnen frei, ein neues Benutzerkonto über den Reiter "Login" anzulegen.
Einmal angelegt können Sie damit einmal gesuchte Orte im Menüpunkt "Meine Orte" abspeichern und anschließend wieder abrufen.
Dazu wird eine e-mail Adresse benötigt.

Sie können entweder über die Karte auf einen Ort Ihrer Wahl klicken oder aber Sie nutzen die "Suche", in der Sie sowohl nach einer Adresse als auch nach den genauen Koordinaten suchen können.
Ausserdem kann hier das genaue Datum und der Zeitpunkt gewählt werden.

Die Suche wird über ein Klicken auf die Lupe in der Menüleiste gestartet.

Sind Sie eingeloggt, können Sie nach abgeschlossener Suche die Ergebnisse unter "Meine Orte" abspeichern. Zudem wird die Möglichkeit geboten, Bilder und Notizen hinzuzufügen.